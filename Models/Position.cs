using System;
using System.ComponentModel.DataAnnotations;

namespace GBMChallenge.Models
{
    public class Position
    {
        [Key]
        public DateTime DateTime { get; set; }
        [Key]
        public long VehicleId { get; set; }

        public double X { get; set; }

        public double Y { get; set; }

    }
}