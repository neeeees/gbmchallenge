namespace GBMChallenge.DTO
{
    public class CreatePosition
    {
        public long VehicleId { get; set; }

        public double X { get; set; }

        public double Y { get; set; }
    }
}