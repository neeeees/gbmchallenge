using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using GBMChallenge.Data;
using GBMChallenge.DTO;
using GBMChallenge.Models;
using GBMChallenge.Services;

namespace GBMChallenge.Controllers
{
    [Authorize]
    [Route("api/[Controller]")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        private readonly PositionContext _context;
        private readonly IPositionService _service;
        //Controller constructor it receives the DbContext by DI
        public VehicleController(PositionContext context, IPositionService service)
        {
            _context = context;
            _service = service;
        }

        [HttpGet("{id}/position")]
        public ActionResult<Position> GetVehiclePosition(long id)
        {
            var lastPosition = _service.GetLastPosition(id);
            if (lastPosition == null)
            {
                return NotFound(new { message = $"there is no position for the given vehicle id: {id}" });
            }

            return lastPosition;
        }

        [HttpPost("/api/[Controller]/position")]
        public ActionResult<Position> PostVehiclePosition(CreatePosition createPosition)
        {
            var createdVehicle = _service.CreateVehiclePosition(createPosition);
            if (createdVehicle == null)
            {
                return NotFound(new { message = $"There is no vehicle with id: {createPosition.VehicleId} saved in the database" });
            }
            
            return CreatedAtAction(nameof(GetVehiclePosition), 
            new { id = createdVehicle.VehicleId }, createdVehicle);
        }
    }
}