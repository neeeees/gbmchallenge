using GBMChallenge.DTO;
using GBMChallenge.Models;

namespace GBMChallenge.Services
{
    public interface IPositionService
    {
        Position GetLastPosition(long vehicleId);

        Position CreateVehiclePosition(CreatePosition cp);
    }
}