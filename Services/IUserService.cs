using System.Collections.Generic;
using GBMChallenge.Models;

namespace GBMChallenge.Services
{
    public interface IUserService
    {
        User Authenticate(string username, string password);
    }
}