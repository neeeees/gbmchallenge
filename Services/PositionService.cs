using System;
using System.Linq;
using GBMChallenge.Data;
using GBMChallenge.DTO;
using GBMChallenge.Models;

namespace GBMChallenge.Services
{
    public class PositionService : IPositionService
    {
        //EF database context
        private readonly PositionContext _context;
        //Position Service constructor that receives EF context
        public PositionService(PositionContext context)
        {
            _context = context;
        }
        
        public Position GetLastPosition(long vehicleId)
        {
            var lastPosition = _context
                                .Positions
                                .LastOrDefault(v => v.VehicleId == vehicleId);
            return lastPosition;
        }

        public Position CreateVehiclePosition(CreatePosition cp)
        {
            var vehicle = _context.Vehicles.FirstOrDefault(v => v.Id == cp.VehicleId);
            if (vehicle == null)
            {
                return null;
            }
            
            var position = new Position {
                    DateTime = DateTime.Now,
                    VehicleId = cp.VehicleId,
                    X = cp.X,
                    Y = cp.Y
                };

            _context.Positions.Add(position);
            _context.SaveChanges();

            return position;
        }
    }
}