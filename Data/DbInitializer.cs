using GBMChallenge.Models;
using System.Linq;

namespace GBMChallenge.Data
{
    public static class DbInitializer
    {
        public static void Initialize (PositionContext context)
        {
            context.Database.EnsureCreated();

            if (context.Vehicles.Any())
            {
                return;
            }

            var i = 1;
            while (i <= 5)
            {
                context.Vehicles.Add(new Vehicle { Id = i, Name = $"Vehicle {i}"});
                i++;
            }
            context.SaveChanges();

            context.Users.Add(new User{
                 Id = 1, 
                 FirstName = "Challenge", 
                 LastName = "User", 
                 Username = "test", 
                 Password = "test" 
            });

            context.SaveChanges();
        }
    }
}