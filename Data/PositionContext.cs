using Microsoft.EntityFrameworkCore;
using GBMChallenge.Models;

namespace GBMChallenge.Data
{
    public class PositionContext : DbContext
    {
        public DbSet<Vehicle> Vehicles { get; set; }

        public DbSet<Position> Positions { get; set; }

        public DbSet<User> Users { get; set; }

        public PositionContext(DbContextOptions<PositionContext> options)
            : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Position>()
                .HasKey(p => new { p.VehicleId, p.DateTime });
        }
    }
}